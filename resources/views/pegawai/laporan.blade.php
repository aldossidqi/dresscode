@extends('adminltepegawai')

@section('content')

@if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
@endif
@if ($message = Session::get('danger')) 
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
@endif
@if($errors->has('id_barang'))
<div class="alert alert-danger" role="alert"> {{$errors->first('id_barang')}} </div>
@endif
@if($errors->has('jumlah_barang_terjual'))
<div class="alert alert-danger" role="alert"> {{$errors->first('jumlah_barang_terjual')}} </div>
@endif
@if($errors->has('jumlah_uang_masuk'))
<div class="alert alert-danger" role="alert"> {{$errors->first('jumlah_uang_masuk')}} </div>
@endif
<title>Laporan</title>
<section class="content-header">
<div class="row"> 
  <div class="col-md-6">
    <h3><b>Laporan Penjualan</b> @if($data1) <button onclick="hapus_laporan('{{$data1->id}}')" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus</button></h3> @endif
  </div>
  <div class="col-md-4">
    
  </div>
  <div class="col-md-2 text-right">
    <button onclick="tambah_laporan()" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah laporan</button>
  </div>
</div>
</section>

<section class="content">
  <div class="row">
        <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped" >
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>ID Barang</th>
                  <th>Jumlah Laku</th>
                  <th>Uang Masuk</th>
                  <th>Tanggal Laku</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 1;
                ?>
                @foreach($data as $dtlaporan)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $dtlaporan->id_barang }}</td>
                  <td>{{ $dtlaporan->jumlah_barang_terjual }}</td>
                  <td>Rp {{ $dtlaporan->jumlah_uang_masuk }}</td>
                  <td><?php 
                  $x =  $dtlaporan->tanggal;
                  $tgl  = substr($x, 8,2);
                  echo $tgl ;
                  $y = substr($x, 5, 2);
                  if ($y == "01" ){
                    echo " Januari ";
                  } elseif ($y == "02") {
                    echo " Februari ";
                  } elseif ($y == "03") {
                    echo " Maret ";
                  } elseif ($y == "04") {
                    echo " April ";
                  } elseif ($y == "05") {
                    echo " Mei ";
                  } elseif ($y == "06") {
                    echo " Juni ";
                  } elseif ($y == "07") {
                    echo " July ";
                  } elseif ($y == "08") {
                    echo " Agustus ";
                  } elseif ($y == "09") {
                    echo " September ";
                  } elseif ($y == "10") {
                    echo " Oktober ";
                  } elseif ($y == "11") {
                    echo " November ";
                  } elseif ($y == "12") {
                    echo " Desember ";
                  }
                  $thn = substr($x, 0, 4);
                  echo $thn;
                  ?></td>
                  
                </tr>
                @endforeach
              </tbody>
            </table>
            
            </div>
            </div>
            </div>


  <!-- Modal Edit laporan -->
    <div class="modal fade" id="tambah_laporan">
      <div class="modal-dialog" role="document" style="width: 60%">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Tambah Data laporan</h4>
            </div>
            <div class="modal-body">
            <form name="form_tambah_laporan" class="form-horizontal" action="{{url('/tambahlaporan')}}" method="post">
           </div>
                <div class="form-group">
                  <label for="inputusername" class="col-sm-2 control-label">ID Barang</label>
                    <div class="col-sm-10">
                      <input type="input" class="form-control" name="id_barang" required="" list="idbrg" placeholder="ID Barang">
                <datalist id="idbrg">
                  @foreach($barangx as $barang)
                  <option value="{{$barang->id_barang}}">{{$barang->id_barang}}</option>
                  @endforeach
                </datalist>
                    </div>
        </div><br><br>
                <div class="form-group">
            <label for="inputnama" class="col-sm-2 control-label">Jumlah Terjual</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" name="jumlah_barang_terjual" required="">
            </div>
        </div><br><br>
        <div class="form-group">
            <label for="inputnama" class="col-sm-2 control-label">Jumlah Uang Masuk</label>
            <div class="col-sm-10">
              <input type="input" class="form-control" name="jumlah_uang_masuk" required="" id="rupiah">
            </div>
        </div><br><br>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
          <input type="submit" class="btn btn-warning" value="Simpan">
        </div>
                {{csrf_field()}}
              </form>
            </div>
        </div>
      </div>
  </div>
  <!-- /.modal -->

 

  <div class="modal fade" id="hapus_laporan">
    <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hapus Data</h4>
            </div>
          <div class="modal-body">
              <p>Apakah yakin ingin menghapus data yang terakhir anda masukan ?</p>
            <form name="form_hapus" class="form-horizontal" action="{{url('/hapuslaporan')}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="id">
              @foreach($data as $dtlaporan)
                <input type="hidden" name="id_barang" value="{{ $dtlaporan->id_barang }}">
                <input type="hidden" name="jumlah_barang_terjual" value="{{ $dtlaporan->jumlah_barang_terjual }}">
              @endforeach
              <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <input type="submit" class="btn btn-danger" value="Hapus">
              </div>
              {{csrf_field()}}
              {{method_field('DELETE')}}
            </form>
          </div>
          </div>
    <!-- /.modal-content -->
    </div>
   <!-- /.modal-dialog -->
   </div>


</section>

@endsection

@section('script')

<script>
  $(function () {
    $('#example1').DataTable({
      'searching'   : true
    })
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
function tambah_laporan(){
      $('#tambah_laporan').modal('show');
    }

 function hapus_laporan(id){
  document.forms['form_hapus']['id'].value=id;
    $('#hapus_laporan').modal('show');
  }

  
  
</script>
<script type="text/javascript">
        
      var rupiah = document.getElementById("rupiah");
      rupiah.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, "Rp. ");
      });
      

      /* Fungsi formatRupiah */
      function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
          split = number_string.split(","),
          sisa = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
          separator = sisa ? "." : "";
          rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? " " + rupiah : "";
      }
    </script>
@endsection
