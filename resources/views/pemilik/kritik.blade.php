@extends('adminlte')

@section('content')

<title>Kritik dan Saran</title>
<section class="content-header">
<div class="row"> 
  <div class="col-md-6">
    <h3><b>Kritik dan Saran</b></h3>
  </div>
  <div class="col-md-4">
    
  </div>
  
</div>
</section>

<section class="content">
  <div class="row">
        <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped" >
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Customer</th>
                  <th>Kritik</th>
                  <th>Saran</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 1;
                ?>
                @foreach($data as $kritik)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $kritik->nama_customer }}</td>
                  <td>{{ $kritik->kritik }}</td>
                  <td>{{ $kritik->saran }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
            
            </div>
            </div>
            </div>



</section>

@endsection

@section('script')

<script>
  $(function () {
    $('#example1').DataTable({
      'searching'   : true
    })
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
@endsection
