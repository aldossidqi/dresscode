<?php

namespace App\Http\Controllers\Pegawai;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LaporanModel;
use App\BarangModel;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller
{
   
    public function lihatlaporan()
    {
        $data = LaporanModel::orderBy('created_at','asc')->get();
        $barangx = BarangModel::orderBy('created_at','asc')->get();
        $data1 = LaporanModel::orderBy('created_at','desc')->first();
        return view ('pegawai/laporan',compact('data','data1','barangx'));
    }

    public function tambahlaporan(Request $request)
    {
        
        date_default_timezone_set('Asia/Jakarta');
             $tgl=date('Y-m-d');
        $hasil =0;
        $barang = BarangModel::where('id_barang', $request->id_barang)->first();
        if(!$barang){
            return redirect('/laporan_pegawai/')->with('danger', 'Data barang dengan ID tersebut tidak ditemukan!');
        } else {
            $hasil = $barang->jumlah_barang - $request->jumlah_barang_terjual;
            if ($hasil < 0){
                return redirect('/laporan_pegawai/')->with('danger', 'Data yang diinputkan salah, stok barang tidak mencukupi!');
            } else {
                $data = [
                    'id_barang'=> $request->id_barang,
                    'jumlah_barang_terjual' => $request->jumlah_barang_terjual,
                    'jumlah_uang_masuk' => $request->jumlah_uang_masuk,
                    'tanggal' => $tgl,
                 ];
                $brg = LaporanModel::create($data);
                BarangModel::where('id_barang',$request->id_barang)->update([
                    "jumlah_barang" => $hasil,
                ]);
                if($brg){
                    $red = redirect('/laporan_pegawai/')->with('success', 'Laporan berhasil ditambahkan!');
                } 
                return $red;
            }
    }
    }

    public function hapuslaporan(Request $request)
    {
        $barang = BarangModel::where('id_barang', $request->id_barang)->first();
        $hasil = $barang->jumlah_barang + $request->jumlah_barang_terjual;
        BarangModel::where('id_barang',$request->id_barang)->update([
            "jumlah_barang" => $hasil,
        ]);
        if(LaporanModel::where("id",$request->id)->delete()){
            return back()->with('success', 'Laporan berhasil di hapus!');
        }else{
            return back()->with('danger', 'Laporan gagal dihapus!');
        }
    }

    public function pemilikliat(Request $request)
    {
        //$data = LaporanModel::orderBy('created_at','asc')->get();
        $data = DB::table('barang_models')->join('laporan_models','barang_models.id_barang','=','laporan_models.id_barang')->get();
        // $total = DB::select('select SUM(jumlah_uang_masuk) as total from laporan_models');
        $jumlahUang = LaporanModel::get();
        foreach ($jumlahUang as $uang ) {
            $jumlah[] = $this->convert_to_angka($uang->jumlah_uang_masuk);
        }
        $jumlahData = count(LaporanModel::get());
        $conRupiah = 0;
        for($i = 0; $i<$jumlahData; $i++){
            $conRupiah = $conRupiah + $jumlah[$i];
        }
        $total = $this->convert_to_rupiah($conRupiah);
        return view ('pemilik/laporan_toko',compact('data','total'));
    }

    function convert_to_rupiah($angka){
        $hasil_rupiah = number_format($angka,0,',','.');
        return $hasil_rupiah;
    }
    function convert_to_angka($nominal){
        $angka = str_replace(".", "", $nominal);
        return $angka;
    }
}
