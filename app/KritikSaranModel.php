<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KritikSaranModel extends Model
{
    protected $fillable = [
        'id_customer','kritik','saran'
    ];
}
