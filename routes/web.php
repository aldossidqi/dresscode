<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 

Route::get('/','LoginController@home');
Route::get('/login','LoginController@login');
Route::post('/loginCheck','LoginController@loginCheck');
Route::get('/logout', 'LoginController@logout'); 

Route::get('/register','LoginController@register');
Route::post('/register','LoginController@store');

Route::get('/home','LoginController@home');
Route::post('/filterBarang','LoginController@filterBarang');
Route::get('/detailbarang_home/{id}','LoginController@detailBarang');


Route::group(["middleware" => "pemilik"], function(){
	Route::post('/filterBarangPemilik','Pemilik\BarangController@filterBarang');

	Route::get('/pemilik','Pemilik\BarangController@lihatbarang');
	Route::get('/barang','Pemilik\BarangController@lihatbarang');
	Route::post('/tambahbarang','Pemilik\BarangController@tambahbarang');
	Route::put('/ubahbarang','Pemilik\BarangController@ubahbarang');
	Route::delete('/hapusbarang','Pemilik\BarangController@hapusbarang');

	Route::get('/akun_pegawai','Pemilik\PegawaiController@lihatpegawai');
	Route::post('/tambah_pegawai','Pemilik\PegawaiController@tambahpegawai');
	Route::put('/ubah_pegawai','Pemilik\PegawaiController@ubahpegawai');
	Route::delete('/hapus_pegawai','Pemilik\PegawaiController@hapuspegawai');

	Route::get('/situs_online_pemilik','Pemilik\BarangController@situsonline');
	Route::get('/detailbarang_p/{id}','Pemilik\BarangController@detailbarang');

	Route::get('/laporan','Pegawai\LaporanController@pemilikliat');

	Route::get('/kirimbarang','Pemilik\LaporanBMController@lihatpengiriman');
	Route::post('/tambahpengiriman','Pemilik\LaporanBMController@tambahpengiriman');
	Route::delete('/hapuspengiriman','Pemilik\LaporanBMController@hapuspengiriman'); 

	Route::get('/verifikasi_pembelian','Pemilik\BarangController@verifikasibeli');
	Route::put('/verifikasibarang','Pemilik\BarangController@verifikasibarang');

	Route::get('/terjual','Pemilik\BarangController@baranglaku');

	Route::get('/lihat_penghasilan_bulanan','Pemilik\LaporanBMController@lihatbulanan');
	Route::post('/pilih_tanggal','Pemilik\LaporanBMController@pilih_tanggal');
	Route::get('/bulanan/{tanggal}','Pemilik\LaporanBMController@bulanan');

	Route::get('/lihat_kritik','Pemilik\BarangController@lihatkritik');


	});

Route::group(["middleware" => "pegawai"], function(){
	Route::get('/pegawai','Pemilik\BarangController@lihataja');
	Route::get('/laporan_pegawai','Pegawai\laporanController@lihatlaporan');
	Route::post('/tambahlaporan','Pegawai\laporanController@tambahlaporan');
	Route::delete('/hapuslaporan','Pegawai\laporanController@hapuslaporan');
	Route::get('/verifikasi_brg_masuk','Pemilik\LaporanBMController@tampilver');
	Route::put('/verifikasipengiriman','Pemilik\LaporanBMController@verifikasipengiriman');

	

	}); 

Route::group(["middleware" => "customer"], function(){
	Route::get('/situs_online/{username}','Customer\BeliController@situsonline');
	Route::post('/filterBarangCustomer','Customer\BeliController@filterBarang');
	Route::get('/datadiri/{username}','Customer\DataDiriController@lihatdatadiri');
	Route::put('/ubahdatadiri/{username}','Customer\DataDiriController@ubahdatadiri');
	Route::get('/pembelian/{username}','Customer\BeliController@pembelian');
	Route::get('/detailbarang/{username}/{id}','Customer\BeliController@detailbarang');
	Route::post('/detailbarang/{username}/{id}','Customer\BeliController@detailbarang');
	Route::get('/belibarang/{username}/{id}','Customer\BeliController@formbelibarang');
	Route::get('/belii/{username}/{id}','Customer\BeliController@pembelian_detail');
	Route::get('/beliii/{username}/{id}','Customer\BeliController@uploadfoto');
	Route::post('/beli/{username}/{id}','Customer\BeliController@belibarang');
	Route::get('/upload_bukti/{username}/{id}','Customer\BeliController@formupload');
	Route::put('/upload/{username}/{id}','Customer\BeliController@uploadbukti');
	Route::delete('/hapusbeli/{username}/{id}','Customer\BeliController@hapusbeli');
	Route::get('/lihatkeranjang/{username}/','Customer\BeliController@lihatkeranjang');
	Route::post('/tambahkeranjang/{username}/{id}','Customer\BeliController@tambahkeranjang');
	Route::put('/updatekeranjang/{username}/','Customer\BeliController@updatekeranjang');
	Route::get('/keranjangbeli/{username}/','Customer\BeliController@lihatkeranjang');
	Route::get('/lihatrekapkeranjang/{username}/','Customer\BeliController@lihatrekapkeranjang');
	Route::put('/rekapkeranjang/{username}/','Customer\BeliController@rekapkeranjang');
	Route::delete('/hapuskeranjang/{username}/{id}','Customer\BeliController@hapuskeranjang');
	Route::get('/uploadkeranjang/{username}/','Customer\BeliController@uploadkeranjang');
	Route::put('/upload/{username}/','Customer\BeliController@upload');

	Route::get('/kritik/{username}','Customer\BeliController@lihatkritik');
	Route::post('/tambah_kritik/','Customer\BeliController@tambahkritik');
	});