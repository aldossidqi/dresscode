<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('transaction_id');
            $table->string('nama_customer');
            $table->string('alamat');
            $table->string('email');
            $table->string('no_hp');
            $table->string('barang_id');
            $table->string('harga');
            $table->string('jumlah_dibeli');
            $table->string('status');
            $table->string('keranjang');
            $table->string('total_harga');
            $table->date('tanggal_beli');
            $table->foreign('user_id')->references('username')->on('users')->onDelete('CASCADE');
            $table->foreign('barang_id')->references('id_barang')->on('barang_models')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_models');
    }
}
